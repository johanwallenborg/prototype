﻿using EcomPrototype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EcomPrototype.ViewModels
{
    public class CartViewModel
    {
        public List<CartItem> Items { get; set; }
        public double GrandTotal { get; set; }
        public int TotalItems { get; set; }

        public void Build(Cart cart)
        {
            Items = cart.Items;
            GrandTotal = cart.GrandTotal;
            TotalItems = cart.TotalItems;
        }

    }
}