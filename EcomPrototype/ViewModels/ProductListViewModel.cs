﻿using EcomPrototype.DAL;
using EcomPrototype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EcomPrototype.ViewModels
{
    public class ProductListViewModel
    {
        readonly EcomPrototypeContext dbContext = new EcomPrototype.DAL.EcomPrototypeContext();

        public List<Category> Categories { get; set; }
        public List<Product> Products { get; set; }
        public ProductListViewModel()
        {
            Categories = new List<Category>();
            Products = new List<Product>();
        }

        public void Build(List<Category> category, List<Product> products)
        {
            Categories = category;
            Products = products;
        }

    }
}