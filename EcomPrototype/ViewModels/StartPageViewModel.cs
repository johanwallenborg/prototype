﻿using EcomPrototype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EcomPrototype.ViewModels
{
    public class StartPageViewModel
    {
        EcomPrototype.DAL.EcomPrototypeContext dbContext = new DAL.EcomPrototypeContext();
        public List<Product> TopNotchProducts = new List<Product>();
        public StartPageViewModel()
        {
            TopNotchProducts = dbContext.Products.Take(4).ToList();
        }
    }
}