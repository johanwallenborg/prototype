﻿using EcomPrototype.DAL;
using EcomPrototype.Models;
using EcomPrototype.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;

namespace EcomPrototype.Controllers
{
    [SessionState(SessionStateBehavior.Required)]
    public class CartController : Controller
    {
        EcomPrototypeContext dbContext = new EcomPrototypeContext();
        private string CartSessionKey = "EcomPrototype_Cart";
      
        public ActionResult Index()
        {
            Cart cart = GetCart();
            return View(cart);
        }

        public ActionResult CheckOut()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CheckOut(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return View(customer);
            }

            return Confirmation(customer);
        }

        public ActionResult Confirmation(Customer customer)
        {
            var cart = GetCart();
            Order order = new Order(cart, customer);

            // Clear Cart
            
            var orderJson = JsonConvert.SerializeObject(order);
            using (WebClient client = new WebClient())
            {
                try
                {
                    string jsonStubUserKey = ConfigurationManager.AppSettings["JsonStubUserKey"];
                    string jsonStubProjectKey = ConfigurationManager.AppSettings["JsonStubProjectKey"];

                    client.Headers.Add("Content-Type", "application/json");
                    client.Headers.Add("JsonStub-User-Key", jsonStubUserKey);
                    client.Headers.Add("JsonStub-Project-Key", jsonStubProjectKey);

                    var result = client.UploadString(@"http://jsonstub.com/order", orderJson);
                    
                    var viewModel = new ConfirmationViewModel();
                    viewModel.OrderId = JObject.Parse(result)["orderId"].ToString();
                    viewModel.OrderJson = JsonConvert.SerializeObject(order);
                    SaveCart(new Cart());
                    return View(viewModel);
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        public ActionResult Add(string articleNumber, int quantity)
        {
            if (quantity > 0)
            {
                var product = dbContext.Products.Where(a => a.ArticleNumber == articleNumber).SingleOrDefault();
                if (product != null)
                {
                    var cart = GetCart();
                    cart.Add(articleNumber, product.Price, product.Name, quantity);
                    SaveCart(cart);
                }
            }
            
            return Redirect(Request.UrlReferrer.ToString());
        }


        public ActionResult Update(string articleNumber,int quantity)
        {
            var cart = GetCart();

            var cartItem = cart.Items.Where(p => p.ArticleNumber == articleNumber).SingleOrDefault();
            if (cartItem != null)
            {
                if (quantity > 0) { 
                    cartItem.Quantity = quantity;
                } else
                {
                    cart.Items.Remove(cartItem);
                }
            }

            SaveCart(cart);

            return Redirect(Request.UrlReferrer.ToString());
        }


        public ActionResult Remove(string articleNumber)
        {
            var cart = GetCart();

            var cartItem = cart.Items.Where(p => p.ArticleNumber == articleNumber).SingleOrDefault();
            if (cartItem != null)
            {
                cart.Items.Remove(cartItem);            
            }

            SaveCart(cart);

            return Redirect(Request.UrlReferrer.ToString());
        }

        [ChildActionOnly]
        public ActionResult _MiniCart()
        {
            var viewModel = new CartViewModel();
            viewModel.Build(GetCart());
            return View(viewModel);
        }

        private Cart GetCart()
        {
            if (Session[CartSessionKey] == null)
                return new Cart();
            var cart = JsonConvert.DeserializeObject<Cart>(Session[CartSessionKey].ToString());
            return cart;
        }

        private void SaveCart(Cart cart)
        {
            JsonSerializer jsonSerializer = new JsonSerializer();
            Session[CartSessionKey] = JsonConvert.SerializeObject(cart);
        }

    }
}