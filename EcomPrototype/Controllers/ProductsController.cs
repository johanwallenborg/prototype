﻿using EcomPrototype.DAL;
using EcomPrototype.Models;
using EcomPrototype.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EcomPrototype.Controllers
{
    public class ProductsController : Controller
    {
        private static EcomPrototypeContext dbContext = new EcomPrototypeContext();

        // GET: Products
        public ActionResult Index()
        {
            var viewModel = new ProductListViewModel();
            viewModel.Build(dbContext.Categories.ToList(), dbContext.Products.ToList());
            return View(viewModel);
        }

        public ActionResult Details(int id)
        {
            var product = dbContext.Products.SingleOrDefault(p => p.ArticleNumber == id.ToString());
            if (product == null)
                return HttpNotFound();
            return View(product);
        }

        public ActionResult Category(int? id)
        {
            var viewModel = new ProductListViewModel();
            if (id != null)
            {
                var category = dbContext.Categories.SingleOrDefault(c => c.Id == id);
                if (category == null)
                {
                    return HttpNotFound();
                }

                
                viewModel.Build(dbContext.Categories.ToList(), dbContext.Products.Where(p => p.Category.Id == category.Id).ToList());

                return View(viewModel);
            }
            else
            {
                viewModel.Build(dbContext.Categories.ToList(), dbContext.Products.ToList());
                return View("Index",viewModel);
            }
        }

        [ChildActionOnly]
        public PartialViewResult Product(Product product)
        {
            return PartialView("_Product",product);
        }

        [ChildActionOnly]
        public ViewResult Categories()
        {
            var categories = dbContext.Categories.ToList();
            return View("_Categories", categories);
        }

        //[ChildActionOnly]
        //public PartialViewResult Product(Product product)
        //{
        //    return PartialView("_Product",product);
        //}
    }
}