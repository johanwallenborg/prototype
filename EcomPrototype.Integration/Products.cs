﻿using EcomPrototype.DAL;
using EcomPrototype.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Text;

namespace Integration
{
    public class Products
    {
        private static EcomPrototypeContext db = new EcomPrototypeContext();
        
        private static List<Product> GetProducts()
        {

            string jsonStubUserKey = ConfigurationManager.AppSettings["JsonStubUserKey"];
            string jsonStubProjectKey = ConfigurationManager.AppSettings["JsonStubProjectKey"];
            var products = new List<Product>();
            using (var client = new WebClient())
            {
                try
                {
                    // Hämta information från JsonStub
                    client.Headers.Add("Content-Type", "application/json");
                    client.Headers.Add("JsonStub-User-Key", jsonStubUserKey);
                    client.Headers.Add("JsonStub-Project-Key", jsonStubProjectKey);
                    string data = client.DownloadString(@"http://jsonstub.com/Products");


                    // De-Serialisera endast om det finns någon inläst data
                    if (!string.IsNullOrEmpty(data))
                    {
                        var obj = JArray.Parse(data);

                        foreach (var item in obj)
                        {
                            products.Add(new Product()
                            {
                                ArticleNumber = item["ArticleNumber"].ToString(),
                                Name = item["Name"].ToString(),
                                Category = new Category() { Name = item["Category"].ToString() },
                                Image = item["Image"].ToString(),
                                Price = Convert.ToInt32(item["Price"])
                            }
                            );
                        }

                        return products;
                    }
                    
                }
                catch (Exception)
                {

                    throw;
                }

                
            }

            return products;

        }

        public static void Import()
        {
            var productsToImport = GetProducts();
            foreach (var p in productsToImport)
            {
                Category category = db.Categories.SingleOrDefault(c => c.Name == p.Category.Name) ?? db.Categories.Local.SingleOrDefault(c => c.Name == p.Category.Name);
                if (category == null)
                {
                    category = db.Categories.Add(p.Category);
                }

                Product product = db.Products.SingleOrDefault(pr => pr.ArticleNumber == p.Category.Name) ?? db.Products.Local.SingleOrDefault(pr => pr.ArticleNumber == p.ArticleNumber);
                if (product == null)
                {
                    p.Category = category;
                    db.Products.Add(p);
                }

            }


            db.SaveChanges();

        }
    }
}
