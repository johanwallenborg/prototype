﻿using EcomPrototype.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;

namespace EcomPrototype.DAL
{
    public class EcomPrototypeContext : DbContext
    {
        public EcomPrototypeContext() : base("EcomPrototypeContext")
        {

        }

        public DbSet<Product> Products { get; set; }
        
        public DbSet<Category> Categories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }


    }
}
