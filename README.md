
This is developed with latest VS17 (15.8.1).

Make a copy of i.e Web.Release.Config and rename it to Web.config. Otherwise an error will be shown in VS because it can't find it.

In some cases you maybe have to run 'Update-Package -ReInstall' from Package Manager Console

## Add your JsonStub-Keys

Add your JsonStub-keys in web.config

<appsettings>
    <add key="JsonStubUserKey" value=""/>
    <add key="JsonStubProjectKey" value=""/>

## Initialize database

1. Restore included database (DB/EcomPrototype.bak) or create a new one on SQL-server (If you create a new DB from scratch, all the tables will be setup when you run the project for the first time)
2. Modify Connectionstring "EcomPrototypeContext" with your credentials
3. Start project with Debug > Start without Debugging
4. Go to http://yoururl/Admin/Products/Import and press "Import" button. If it should show a 404-error you probably forgot to change the JsonStub-keys in web.config.

To verify that everything went well. go to http://yoururl. If you see products you're all set.

Good luck! :)