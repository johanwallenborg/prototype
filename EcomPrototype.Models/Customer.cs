﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcomPrototype.Models
{
    public class Customer
    {
        [Required]
        [JsonProperty("firstname")]
        [DisplayName("Förnamn")]
        public string FirstName { get; set; }
        [Required]
        [DisplayName("Efternamn")]
        [JsonProperty("lastname")]
        public string LastName { get; set; }
        [JsonProperty("address")]
        [Required]
        [DisplayName("Adress")]
        public string Address { get; set; }
        [Required]
        [DisplayName("Postnummer")]
        [JsonProperty("zipcode")]
        public string Zip { get; set; }
        [Required]
        [DisplayName("Ort")]
        [JsonProperty("city")]
        public string City { get; set; }
        [Required]
        [DisplayName("Telefon")]
        [JsonProperty("phone")]
        public string PhoneNumber { get; set; }
        [Required]
        [DisplayName("Email")]
        [JsonProperty("email")]
        public string Email { get; set; }
    }


}
