﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcomPrototype.Models
{
    public class CartItem
    {
        public string ArticleNumber { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
    }

    public class Cart
    {
        [JsonProperty("orderLines")]
        public List<CartItem> Items { get; set; }
        public Cart()
        {
            Items = new List<CartItem>();
        }
        [JsonProperty("totalPrice")]
        public double GrandTotal
        {
            get
            {
                double grandTotal = 0;

                Items.ForEach(item =>
                {
                    grandTotal += item.Price * item.Quantity;
    
                });

                return grandTotal;
            }
        }
        [JsonIgnore]
        public int TotalItems
        {
            get
            {
                int totalItems = 0;

                Items.ForEach(item =>
                {
                    totalItems += item.Quantity;

                });

                return totalItems;
            }
        }

        public void Add(string articleNumber, double articlePrice, string articleName, int quantity)
        {
            try
            {
                var item = Items.Where(i => i.ArticleNumber == articleNumber).SingleOrDefault();
                if (item == null)
                {

                    Items.Add(new CartItem() { ArticleNumber = articleNumber, Price = articlePrice, Name= articleName, Quantity = quantity });
                } else
                {
                    item.Quantity += quantity;
                }                
                
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        public void AddItem(CartItem item)
        {
            try
            {
                if (item != null)
                {
                    Items.Add(item);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }


        public void Remove(string articleNumber)
        {
            try
            {
                var item = Items.Where(c => c.ArticleNumber == articleNumber).SingleOrDefault();

                if (item != null)
                {
                    Items.Remove(item);
                }
            }
            catch (Exception)
            {

                throw;
            }
            
        }




    }
}
