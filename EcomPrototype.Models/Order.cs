﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcomPrototype.Models
{
    public class Order
    {
        [JsonProperty("order")]
        public Cart Cart { get; set; }
        public Customer Customer {get;set;}

        public Order(Cart cart, Customer customer)
        {
            Cart = cart;
            Customer = customer;
        }
    }
}
